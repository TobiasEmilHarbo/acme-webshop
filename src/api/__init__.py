from flask_restful import Api
from shop import app
from .resources import ProductRes, CartRes, CartItemsRes
from flask_cors import CORS

api = Api(app)
CORS(app)

api.add_resource(ProductRes, '/api/products')
api.add_resource(CartItemsRes, '/api/carts/items')
api.add_resource(CartRes, '/api/carts/items/<int:id>')
