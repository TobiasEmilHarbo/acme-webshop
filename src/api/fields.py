from flask_restful import fields

productfields = {
    'id': fields.Integer,
    'name': fields.String,
    'image': fields.String,
    'stock': fields.Integer,
    'price': fields.Integer
}