from flask_restful import Resource, fields, marshal_with
from flask import request, redirect, render_template, url_for, make_response
from shop import db
from shop.forms import ProductForm
from shop.models import Products
from shop.services import Carts
from .fields import productfields
import json

# /api/products
class ProductRes(Resource):
    @marshal_with(productfields)
    def get(self):
        products = Products.query.all()
        return products

    @marshal_with(productfields)
    def post(self):
        form = ProductForm()
        if form.validate_on_submit():

            newProduct = Products(name=form.data['name'], description=form.data['description'], image=form.data['image'], stock=form.data['stock'], price=form.data['price'])

            db.session.add(newProduct)
            db.session.commit()

            return newProduct, 200
        else: 
            return '', 422

# /api/carts/items/<int:id>
class CartRes(Resource):
    def post(self, id):

        data = request.get_json()
        if data is None:
            return '', 422 # inputs missing

        action = data['action']

        if action is None:
            return 'Inputs missing', 422
        
        productId = str(id)
        product = Products.query.get(productId)

        if product is None:
            return 'No product found', 404

        cart = Carts.get_contents()

        cartItem = cart.get(productId)

        if cartItem is None:
            cartItem = {
                'quantity': 0,
                'price': product.price
            }

        productQuantity = cartItem.get('quantity', 0)

        if action == 'add':
            newQuantity = productQuantity + 1
        elif action == 'sub':
            newQuantity = productQuantity - 1
        else:
            return 'Inputs missing', 422

        if newQuantity < 1:
            cart.pop(productId, None)
        else:
            cartItem['quantity'] = newQuantity
            cart[productId] = cartItem

        response = make_response(json.dumps(cart, separators=(',', ':')))
        
        response.headers['Content-Type'] = 'application/json'
        response.set_cookie('cart', json.dumps(cart, separators=(',', ':')))

        return response

    def delete(self, id):
        cart = Carts.get_contents()
        cart.pop(str(id), None)

        response = make_response(json.dumps(cart, separators=(',', ':')))
        
        response.headers['Content-Type'] = 'application/json'
        response.set_cookie('cart', json.dumps(cart, separators=(',', ':')))

        return response

# /api/carts/items
class CartItemsRes(Resource):
    @marshal_with(productfields)
    def get(self):
        products = Carts.get_items()
        return products