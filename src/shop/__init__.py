from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///store.db'

app.config['SECRET_KEY'] = '5j4kl5j3lkjl345343nmkn323m4232'

db = SQLAlchemy(app)

from shop import routes
