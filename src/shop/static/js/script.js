const updateCartCountView = (cart) => {
  const cartItemCount = $('a#cart span.item-count');
  if(!cartItemCount) return;

  if(!cart) return;
  const productIds = Object.keys(cart);

  if(!productIds.length) {
    cartItemCount.html(`(0)`);
    return
  };

  let count = 0;

  productIds.forEach(id => {
    count += cart[id]['quantity'];
  })

  cartItemCount.html(`(${count})`);
}

const updateItemCounterView = (cart, productId) => {
  const itemQuantityView = $(`div.cart-page div#product-${productId}`);
  const subTotalView = $(`footer div.subtotal span`);

  if(!itemQuantityView) return;

  const items = Object.values(cart)

  if(!!subTotalView && items.length > 0) {
    const subtotal = items.map((item) => item['quantity'] * item['price']).reduce((prev, curr)=> prev + curr);
    subTotalView.html(subtotal)
  }
  else subTotalView.remove()

  if(!cart[productId]) return itemQuantityView.remove();

  const quantity = cart[productId]['quantity']
  const price = cart[productId]['price']

  const counter = itemQuantityView.find('div.item-counter .count')
  const totalPrice = itemQuantityView.find('div.col-total span')
  
  totalPrice.html(price*quantity)
  counter.html(quantity);
}

const cart = getCookie('cart', true)
updateCartCountView(cart)

function getCookie(name, json) {
  const cookieArr = document.cookie.split(";");
  
  for(let i = 0; i < cookieArr.length; i++) {
    const cookiePair = cookieArr[i].split("=");
    window.pair = (cookiePair)
    if(name == cookiePair[0].trim()) {
      let content = decodeURIComponent(cookiePair[1]);
      if(json)
        return JSON.parse(JSON.parse(content.replace(/\\054/g, ',')));
      return content
    }
  }
  return null;
}

const cartEditProductCount = (productId, action) => {
  $.ajax({
    type: "POST",
    url: `http://localhost:5000/api/carts/items/${productId}`,
    data: JSON.stringify({ action: action }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: (data) => {
      const cart = getCookie('cart', true)
      updateCartCountView(cart)
      updateItemCounterView(cart, productId)
    },
    error: (error) => {
      console.log('Error', error)
    }
  });
}

const cartRemoveProduct = (productId) => {
  $.ajax({
    type: "DELETE",
    url: `http://localhost:5000/api/carts/items/${productId}`,
    success: (data) => {
      const cart = getCookie('cart', true)
      updateCartCountView(cart)
      updateItemCounterView(cart, productId)
    },
    error: (error) => {
      console.log('Error', error)
    }
  });
}

const addProductForm = $('form.add-product-form');

addProductForm.submit((event) => {
  event.preventDefault();
  const data = addProductForm.serialize()
  
  $.post("http://localhost:5000/api/products", data)
  .done(() => {
    addProductForm.find('input[type=text]').val('');
  })
  .fail((xhr, status, error) => {
    console.log('Error', error)
  });
});