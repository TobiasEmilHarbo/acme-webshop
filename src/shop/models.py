from shop import db
from sqlalchemy import ForeignKey

orderLines = db.Table('order_lines',
    db.Column('productId', db.Integer, db.ForeignKey('products.id'), primary_key=True),
    db.Column('orderId', db.Integer, db.ForeignKey('orders.id'), primary_key=True),
)

class Products(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(120), nullable=False)
    image = db.Column(db.String(400), nullable=False)
    stock = db.Column(db.Integer, nullable=False)
    price = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Product %r>' % self.name

class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    address = db.Column(db.String(120), nullable=False)
    city = db.Column(db.String(120), nullable=False)
    products = db.relationship('Products', secondary=orderLines, lazy='subquery',
            backref=db.backref('orders', lazy=True))

    def __repr__(self):
        return '<Order %r>' % self.name