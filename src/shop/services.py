from .models import Products
from flask import request
import json

class Carts():
    def __repr__(self):
        return '<Cart %r>' % self.id

    @classmethod
    def get_contents(self):
        cartString = request.cookies.get('cart')

        if cartString is None:
            cart = {}
        else:
            cart = json.loads(cartString)
        return cart

    @classmethod
    def get_items(self):
        cart = Carts.get_contents()
        ids = tuple(cart.keys())

        products = Products.query.filter(Products.id.in_(ids)).all()
        return products
    
    @classmethod
    def get_total(cls):
        cart = Carts.get_contents()

        total = 0
        for productId in cart:
            total += cart[productId]['quantity'] * cart[productId]['price']

        return total