from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, DecimalField
from wtforms.validators import DataRequired

class ProductForm(FlaskForm):
    name = StringField(id='name', label='Name', validators=[DataRequired()])
    description = StringField(id='description', label='Description', validators=[DataRequired()])
    image = StringField(id='image-url', label='Image URL', validators=[DataRequired()])
    stock = IntegerField(id='stock', label='Stock', validators=[DataRequired()])
    price = IntegerField(id='price', label='Price', validators=[DataRequired()])

class OrderForm(FlaskForm):
    name = StringField(id='name', label='Name', validators=[DataRequired()])
    email = StringField(id='email', label='Email', validators=[DataRequired()])
    address = StringField(id='address', label='Address', validators=[DataRequired()])
    city = StringField(id='city', label='City', validators=[DataRequired()])