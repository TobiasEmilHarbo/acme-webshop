from flask import Flask, flash, url_for, redirect, request, render_template, make_response
from shop import app, db
from .forms import ProductForm, OrderForm
from .models import Products, Orders, orderLines
from .services import Carts
from sqlalchemy.sql import case

import json

name = 'ACME Webshop'

@app.route('/')
def index():
    products = Products.query.order_by(Products.id).all()
    return render_template('index.html', title=name, name=name, products=products)

@app.route('/admin')
def admin():
    form = ProductForm()
    return render_template('admin.html', title='Admin panel', name=name, form=form)

@app.route('/admin/orders')
def admin_orders():

    orders = Orders.query.order_by(Orders.id).all()
    return render_template('orders.html', title='Orders', name=name, orders=orders)

@app.route('/admin/products')
def admin_products():

    products = Products.query.order_by(Products.id).all()
    return render_template('products.html', title='Orders', name=name, products=products)

@app.route('/cart')
def cart():
    cart = Carts.get_contents()
    products = Carts.get_items()
    total = Carts.get_total()

    return render_template('/cart.html', title='Cart', name=name, products=products, cart=cart, total=total)

@app.route('/cart/checkout')
def checkout():
    cart = Carts.get_contents()
    products = Carts.get_items()

    print(cart)

    if not bool(cart):
        return redirect(url_for('cart'))

    total = Carts.get_total()
    form = OrderForm()

    return render_template('checkout.html', title='Checkout', name=name, products=products, cart=cart, total=total, form=form)

@app.route('/orders/<int:id>', methods=['GET'])
def order(id):
    order = Orders.query.get(id)
    if not bool(order):
        return '', 404
    products = order.products

    return render_template('order.html', title='Order #'+str(order.id), order=order, products=products)

@app.route('/orders', methods=['POST'])
def orders():
    form = OrderForm()

    if form.validate_on_submit():

        cart = Carts.get_contents()

        if not bool(cart):
            return redirect(url_for('index'))

        name = form.data['name']
        email = form.data['email']
        address = form.data['address']
        city = form.data['city']

        order = Orders()
        order.name = name
        order.email = email
        order.address = address
        order.city = city

        ids = tuple(cart.keys())
    
        products = Products.query.filter(Products.id.in_(ids)).all()

        order.products.extend(products)

        db.session.add(order)

        updatedItemStock = {}
        for product in products:
            updatedItemStock[product.id] = product.stock - cart[str(product.id)]['quantity']

        Products.query.filter(
            Products.id.in_(updatedItemStock)
        ).update({
            Products.stock: case(
                updatedItemStock,
                value=Products.id,
            )
        }, synchronize_session=False)
        
        db.session.commit()

        response = make_response(redirect(url_for('order', id=order.id)))
        response.set_cookie('cart', json.dumps({}, separators=(',', ':')))
        return response
    else:
        flash('Form validation error')
        return redirect(url_for('checkout'))

@app.route('/products/<int:id>')
def product(id):
    product = Products.query.get(id)

    if not bool(product):
        return '', 404
    return render_template('product.html', title=product.name, name=name, product=product)

if __name__ == "__main__":
    app.run(debug=True)