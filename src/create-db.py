from shop import db, app
from shop.models import Products, Orders, orderLines
from sqlalchemy import inspect
inspector = inspect(db.engine)
schemas = inspector.get_schema_names()

# Products.__table__.drop(db.engine)
Orders.__table__.drop(db.engine)
orderLines.drop(db.engine)

db.create_all()

for schema in schemas:
    print("schema: %s" % schema)
    for table_name in inspector.get_table_names(schema=schema):
        print("table: %s" % table_name)
        for column in inspector.get_columns(table_name, schema=schema):
            print("Column: %s" % column)